// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShoppingCarts.h"
#include "ShoppingCartsHud.h"
#include "ShoppingCartsPawn.h"
#include "GameFramework/WheeledVehicle.h"
#include "RenderResource.h"
#include "Shader.h"
#include "Engine/Canvas.h"
#include "Vehicles/WheeledVehicleMovementComponent.h"
#include "Engine/Font.h"
#include "CanvasItem.h"
#include "Engine.h"
#include "PowerupType.h"
#include "BoostPowerupComponent.h"

// Needed for VR Headset
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#endif // HMD_MODULE_INCLUDED 

#define LOCTEXT_NAMESPACE "VehicleHUD"

AShoppingCartsHud::AShoppingCartsHud()
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = Font.Object;
}

void AShoppingCartsHud::DrawHUD()
{
	Super::DrawHUD();

	// Calculate ratio from 720p
	const float HUDXRatio = Canvas->SizeX / 1280.f;
	const float HUDYRatio = Canvas->SizeY / 720.f;

	bool bWantHUD = true;
#if HMD_MODULE_INCLUDED
	if (GEngine->HMDDevice.IsValid() == true)
	{
		bWantHUD = GEngine->HMDDevice->IsStereoEnabled();
	}
#endif // HMD_MODULE_INCLUDED
	// We dont want the onscreen hud when using a HMD device	
	if (bWantHUD == true)
	{
		// Get our vehicle so we can check if we are in car. If we are we don't want onscreen HUD
		AShoppingCartsPawn* Vehicle = Cast<AShoppingCartsPawn>(GetOwningPawn());
		if ((Vehicle != nullptr) && (Vehicle->bInCarCameraActive == false))
		{
			if (Vehicle->Victory)
			{
				FVector2D ScaleVec(HUDYRatio * 5.f, HUDYRatio * 5.f);
				FCanvasTextItem VictoryItem(FVector2D(HUDXRatio * 320.f, HUDYRatio * 180.f), FText::FromString(TEXT("VICTORY!")), HUDFont, FLinearColor::Green);
				VictoryItem.Scale = ScaleVec;
				Canvas->DrawItem(VictoryItem);
			}
			else
			{
				FVector2D ScaleVec(HUDYRatio * 1.4f, HUDYRatio * 1.4f);


				if (Vehicle->WantedItems.Num())
				{
					for (int k = 0; k < Vehicle->WantedItems.Num(); ++k)
					{
						FCanvasTextItem ListItem(FVector2D(HUDXRatio * 100.f, HUDYRatio * 50.f + (k*45.f)), FText::FromString(Vehicle->WantedItems[k]), HUDFont, FLinearColor::Yellow);
						ListItem.Scale = ScaleVec;
						Canvas->DrawItem(ListItem);
					}
				}
				else
				{
					FCanvasTextItem LeaveItem(FVector2D(HUDXRatio * 100.f, HUDYRatio * 140.f), FText::FromString(TEXT("Head to the finish!")), HUDFont, FLinearColor::Green);
					LeaveItem.Scale = ScaleVec;
					Canvas->DrawItem(LeaveItem);
				}

				FCanvasTextItem Current(FVector2D(HUDXRatio * 805.f, HUDYRatio * 410.f), FText::FromString(Vehicle->ItemString), HUDFont, FLinearColor::White);
				Current.Scale = ScaleVec;
				Canvas->DrawItem(Current);

				// Speed
				FCanvasTextItem SpeedTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 455.f), Vehicle->SpeedDisplayString, HUDFont, FLinearColor::White);
				SpeedTextItem.Scale = ScaleVec;
				Canvas->DrawItem(SpeedTextItem);

				// Gear
				FCanvasTextItem GearTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 500.f), Vehicle->GearDisplayString, HUDFont, Vehicle->bInReverseGear == false ? Vehicle->GearDisplayColor : Vehicle->GearDisplayReverseColor);
				GearTextItem.Scale = ScaleVec;
				Canvas->DrawItem(GearTextItem);

				FCanvasTextItem WeightItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 545.f), FText::AsNumber(Vehicle->GetVehicleMovementComponent()->Mass), HUDFont, FLinearColor::Blue);
				WeightItem.Scale = ScaleVec;
				Canvas->DrawItem(WeightItem);

				bool HasBoost = false;
//				for (UCartPowerupComponent *Comp : Vehicle->GetAttachedPowerupComponents())
//				{
//					if (Comp && Comp->IsA(UBoostPowerupComponent::StaticClass()))
//					{
//						HasBoost = true;
//					}
//				}
				if (HasBoost)
				{
					FCanvasTextItem BoostItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 95.f), FText::FromString(TEXT("[BOOST]")), HUDFont, FLinearColor::Green);
					BoostItem.Scale = ScaleVec;
					Canvas->DrawItem(BoostItem);
				}
				else
				{
					FCanvasTextItem BoostItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 95.f), FText::FromString(TEXT("NO BOOST")), HUDFont, FLinearColor::Red);
					BoostItem.Scale = ScaleVec;
					Canvas->DrawItem(BoostItem);
				}

				if (Vehicle->Health > 50)
				{
					FString tempString = TEXT("HP: ") + FString::FromInt(Vehicle->Health) + TEXT("/") + FString::FromInt(100);

					FCanvasTextItem HealthItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 45.f), FText::FromString(tempString), HUDFont, FLinearColor::Green);
					HealthItem.Scale = ScaleVec;
					Canvas->DrawItem(HealthItem);
				}
				else if (Vehicle->Health > 20)
				{
					FString tempString = TEXT("HP: ") + FString::FromInt(Vehicle->Health) + TEXT("/") + FString::FromInt(100);

					FCanvasTextItem HealthItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 45.f), FText::FromString(tempString), HUDFont, FLinearColor::Yellow);
					HealthItem.Scale = ScaleVec;
					Canvas->DrawItem(HealthItem);
				}
				else if (Vehicle->Health > 0)
				{
					FString tempString = TEXT("HP: ") + FString::FromInt(Vehicle->Health) + TEXT("/") + FString::FromInt(100);

					FCanvasTextItem HealthItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 45.f), FText::FromString(tempString), HUDFont, FLinearColor::Red);
					HealthItem.Scale = ScaleVec;
					Canvas->DrawItem(HealthItem);
				}
				else
				{
					FString tempString = TEXT("RIP - AWAITING RESPAWN");

					FCanvasTextItem HealthItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 45.f), FText::FromString(tempString), HUDFont, FLinearColor::Red);
					HealthItem.Scale = ScaleVec;
					Canvas->DrawItem(HealthItem);
				}
			}
		}
	}
}


#undef LOCTEXT_NAMESPACE

#pragma once

#include "CartPowerupComponent.h"
#include "BoostPowerupComponent.generated.h"

UCLASS()
class SHOPPINGCARTS_API UBoostPowerupComponent : public UCartPowerupComponent
{
	GENERATED_BODY()

	UBoostPowerupComponent();
	
	void BeginPlay() override;

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(Category = Boost, EditAnywhere)
	float BoostImpulseMagnitude = 1000.f;

	UPROPERTY(Category = Boost, EditAnywhere)
	float BoostFOV = 100.f;

	UPROPERTY(Category = Boost, EditAnywhere)
	float BoostFOVDamping = 4.f;

	UPROPERTY(Category = Boost, EditAnywhere, BlueprintReadOnly)
	float MaxBoostLevel = 100.f;

	UPROPERTY(Category = Boost, EditAnywhere, BlueprintReadOnly)
	float BoostConsumptionRate = 30.f;

	UPROPERTY(Category = Boost, EditAnywhere)
	float BoostRechargeRate = 10.f;

	UPROPERTY(Category = Boost, EditAnywhere)
	UParticleSystem *NitroParticlesTemplate;
    
    UFUNCTION(Category = Boost, BlueprintCallable)
    float GetBoostPercent(){return (BoostLevel/MaxBoostLevel);}

	bool IsBoosting() const { return bIsBoosting; }
	void SetIsBoosting(bool InBoosting);

protected:
	UPROPERTY(Category = Boost, VisibleAnywhere, Transient)
	bool bIsBoosting = false;

	UPROPERTY(Category = Boost, VisibleAnywhere, BlueprintReadOnly, Transient)
	float BoostLevel = MaxBoostLevel;

	UParticleSystemComponent *LeftParticleSystem, *RightParticleSystem;
};

// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShoppingCarts.h"
#include "ShoppingCartsPawn.h"
#include "ShoppingCartsWheelFront.h"
#include "ShoppingCartsWheelRear.h"
#include "ShoppingCartsHud.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Vehicles/WheeledVehicleMovementComponent4W.h"
#include "Engine/SkeletalMesh.h"
#include "Engine.h"
#include "PowerupType.h"
#include "BoostPowerupComponent.h"
#include "TurretPowerupComponent.h"
#include "MissilePowerupComponent.h"

// Needed for VR Headset
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#endif // HMD_MODULE_INCLUDED

const FName AShoppingCartsPawn::LookUpBinding("LookUp");
const FName AShoppingCartsPawn::LookRightBinding("LookRight");

#define LOCTEXT_NAMESPACE "VehiclePawn"

static TMap<FString, UMaterial*> ItemMaterials;
static int SpawnNumber = 0;

AShoppingCartsPawn::AShoppingCartsPawn()
{
	// Car mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> CarMesh(TEXT("/Game/Cart/Cart.Cart"));
	GetMesh()->SetSkeletalMesh(CarMesh.Object);

	static ConstructorHelpers::FClassFinder<UObject> AnimBPClass(TEXT("/Game/Cart/Cart_Skeleton_AnimBlueprint"));
	GetMesh()->SetAnimInstanceClass(AnimBPClass.Class);

	static ConstructorHelpers::FClassFinder<AActor> DeathClass(TEXT("/Game/StarterContent/Blueprints/Blueprint_Effect_Explosion"));
	DeathEffectClass = DeathClass.Class;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> ItemSpawnMeshFinder(TEXT("/Engine/BasicShapes/Cube.Cube"));
	ItemSpawnMesh = ItemSpawnMeshFinder.Object;

	ConstructorHelpers::FObjectFinder<USoundCue> EngineSoundFinder((SpawnNumber++ % 2) == 0 ? TEXT("/Game/Cart/Sounds/CartMotor1") : TEXT("/Game/Cart/Sounds/CartMotor2"));
	EngineSound = EngineSoundFinder.Object;

	// Simulation
	UWheeledVehicleMovementComponent4W* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(GetVehicleMovement());
	Vehicle4W->Mass = 1000.f;

	check(Vehicle4W->WheelSetups.Num() == 4);

	Vehicle4W->WheelSetups[0].WheelClass = UShoppingCartsWheelFront::StaticClass();
	Vehicle4W->WheelSetups[0].BoneName = FName("Wheel_Front_Left");
	Vehicle4W->WheelSetups[0].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[1].WheelClass = UShoppingCartsWheelFront::StaticClass();
	Vehicle4W->WheelSetups[1].BoneName = FName("Wheel_Front_Right");
	Vehicle4W->WheelSetups[1].AdditionalOffset = FVector(0.f, 12.f, 0.f);

	Vehicle4W->WheelSetups[2].WheelClass = UShoppingCartsWheelRear::StaticClass();
	Vehicle4W->WheelSetups[2].BoneName = FName("Wheel_Rear_Left");
	Vehicle4W->WheelSetups[2].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[3].WheelClass = UShoppingCartsWheelRear::StaticClass();
	Vehicle4W->WheelSetups[3].BoneName = FName("Wheel_Rear_Right");
	Vehicle4W->WheelSetups[3].AdditionalOffset = FVector(0.f, 12.f, 0.f);

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->TargetOffset = FVector(0.f, 0.f, 180.f);
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 220.f;
	SpringArm->bEnableCameraRotationLag = true;
	SpringArm->CameraRotationLagSpeed = 7.f;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;

	// Create camera component
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->SetRelativeRotation(FRotator(-15.f, 0.f, 0.f));
	Camera->bUsePawnControlRotation = false;
	Camera->FieldOfView = 70.f;

	// Create In-Car camera component
	InternalCameraOrigin = FVector(0.f, 0.f, 1500.0f);

	InternalCameraBase = CreateDefaultSubobject<USceneComponent>(TEXT("InternalCameraBase"));
	InternalCameraBase->SetRelativeLocation(InternalCameraOrigin);
	InternalCameraBase->SetupAttachment(GetMesh());

	GearDisplayReverseColor = FColor(255, 0, 0, 255);
	GearDisplayColor = FColor(255, 255, 255, 255);

	InternalCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("InternalCamera"));
	InternalCamera->bUsePawnControlRotation = false;
	InternalCamera->FieldOfView = 70.f;
	InternalCamera->SetupAttachment(InternalCameraBase);
	InternalCamera->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));

	bInReverseGear = false;

	ItemString = TEXT("");
	Populate();
	Victory = false;
	//Blowup = false;
}

void AShoppingCartsPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AShoppingCartsPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShoppingCartsPawn::MoveRight);
	PlayerInputComponent->BindAxis("LookUp");
	PlayerInputComponent->BindAxis("LookRight");

	PlayerInputComponent->BindAction("Collect", IE_Pressed, this, &AShoppingCartsPawn::OnCollect);
	PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &AShoppingCartsPawn::OnHandbrakePressed);
	PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &AShoppingCartsPawn::OnHandbrakeReleased);
	PlayerInputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AShoppingCartsPawn::OnToggleCamera);

	PlayerInputComponent->BindAction("Pickup", IE_Pressed, this, &AShoppingCartsPawn::OnCollect);
	PlayerInputComponent->BindAction("Boost", IE_Pressed, this, &AShoppingCartsPawn::OnBoostStart);
	PlayerInputComponent->BindAction("Boost", IE_Released, this, &AShoppingCartsPawn::OnBoostStop);
	PlayerInputComponent->BindAction("Reset", IE_Pressed, this, &AShoppingCartsPawn::OnReset);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShoppingCartsPawn::OnFireStart);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShoppingCartsPawn::OnFireStop);
	PlayerInputComponent->BindAction("MainMenu", IE_Pressed, this, &AShoppingCartsPawn::MainMenu);

	//PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AShoppingCartsPawn::OnResetVR);
}

void AShoppingCartsPawn::MoveForward(float Val)
{
	if (Health > 0)
	{
		GetVehicleMovementComponent()->SetThrottleInput(Val);
	}
}

void AShoppingCartsPawn::MoveRight(float Val)
{
	if (Health > 0)
	{
		GetVehicleMovementComponent()->SetSteeringInput(Val);
	}
}

void AShoppingCartsPawn::OnHandbrakePressed()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AShoppingCartsPawn::OnHandbrakeReleased()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AShoppingCartsPawn::OnToggleCamera()
{
	EnableIncarView(!bInCarCameraActive);
}

void AShoppingCartsPawn::OnBoostStart()
{
	if (Health > 0 && BoostPowerup)
	{
		BoostPowerup->SetIsBoosting(true);
	}
}

void AShoppingCartsPawn::OnBoostStop()
{
	if (Health > 0 && BoostPowerup)
	{
		BoostPowerup->SetIsBoosting(false);
	}
}

void AShoppingCartsPawn::OnFireStart()
{
	if (Health > 0)
	{
		if (TurretPowerup)
		{
			TurretPowerup->StartFire();
		}
		if (MissilePowerup)
		{
			MissilePowerup->Launch();
		}
	}
}

void AShoppingCartsPawn::OnFireStop()
{
	if (Health > 0 && TurretPowerup)
	{
		TurretPowerup->StopFire();
	}
}

void AShoppingCartsPawn::OnReset()
{
	if (Health > 0 && GetMesh()->GetComponentVelocity().Size() < 10.f)
	{
		// Move up and rotate upright to hopefully fix things
		SetActorLocation(GetActorLocation() + FVector::UpVector * RespawnUpOffset, false, nullptr, ETeleportType::TeleportPhysics);
		FRotator mRotator = GetActorRotation();
		mRotator.Roll = 0.f;
		mRotator.Pitch = 0.f;
		SetActorRotation(mRotator, ETeleportType::TeleportPhysics);
	}
}

void AShoppingCartsPawn::EnableIncarView(const bool bState, const bool bForce)
{
	if ((bState != bInCarCameraActive) || ( bForce == true ))
	{
		bInCarCameraActive = bState;

		if (bState == true)
		{
			OnResetVR();
			Camera->Deactivate();
			InternalCamera->Activate();
		}
		else
		{
			InternalCamera->Deactivate();
			Camera->Activate();
		}
	}
}


void AShoppingCartsPawn::Tick(float Delta)
{
	Super::Tick(Delta);

	// Add some physics
	//float turningAmount = FVector::DotProduct(GetActorRightVector(), GetVelocity());
	//UE_LOG(LogTemp, Log, TEXT("%f"), turningAmount);

	// Setup the flag to say we are in reverse gear
	bInReverseGear = GetVehicleMovement()->GetCurrentGear() < 0;

	// Update the strings used in the hud (incar and onscreen)
	UpdateHUDStrings();

	// Engine pitch change
	float NewPitch = FMath::GetMappedRangeValueClamped(FVector2D(0, 80.f), FVector2D(0.7f, 2.f), GetSpeed());
	EngineAudioComponent->SetPitchMultiplier(NewPitch);

	bool bHMDActive = false;
#if HMD_MODULE_INCLUDED
	if ((GEngine->HMDDevice.IsValid() == true) && ((GEngine->HMDDevice->IsHeadTrackingAllowed() == true) || (GEngine->IsStereoscopic3D() == true)))
	{
		bHMDActive = true;
	}
#endif // HMD_MODULE_INCLUDED
	if (bHMDActive == false)
	{
		if ( (InputComponent) && (bInCarCameraActive == true ))
		{
			FRotator HeadRotation = InternalCamera->RelativeRotation;
			HeadRotation.Pitch += InputComponent->GetAxisValue(LookUpBinding);
			HeadRotation.Yaw += InputComponent->GetAxisValue(LookRightBinding);
			InternalCamera->RelativeRotation = HeadRotation;
		}
	}

	if (GetActorLocation().Z < -5000.f)
	{
		/*if (!Blowup)
		{
			FVector EffectSpawnLocation = GetActorLocation();
			GetWorld()->SpawnActor(DeathEffectClass, &EffectSpawnLocation);

			// Toss into darkness
			GetMesh()->AddImpulse(FVector::UpVector * -800.f, NAME_None, true);
			FRandomStream random;
			random.GenerateNewSeed();
			GetMesh()->AddAngularImpulse(random.GetUnitVector() * 20.f, NAME_None, true);
			Blowup = true;
		}*/

		if (WantedItems.Num() == 0 && !Cast<AShoppingCartsPawn>(GetEnemy())->Victory)
			Victory = true;
		else
		{
			Health = 0.0f;
			if(GetActorLocation().Z < -12000.f)
				AfterDeath();
		}
	}
	if (Cast<AShoppingCartsPawn>(GetEnemy())->Victory)
		Cast<AShoppingCartsPawn>(GetEnemy())->GetVehicleMovementComponent()->Deactivate();
	
}

void AShoppingCartsPawn::BeginPlay()
{
	Super::BeginPlay();

	bool bEnableInCar = false;
#if HMD_MODULE_INCLUDED
	bEnableInCar = UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled();
#endif // HMD_MODULE_INCLUDED
	EnableIncarView(bEnableInCar,true);

	// Start audio
	EngineAudioComponent = UGameplayStatics::SpawnSoundAttached(EngineSound, GetMesh(), NAME_None, FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);

	// Add postprocess shelf highlight effect to cameras
	int id = GetId();
	const TCHAR *BlendableName = id == 0 ? TEXT("/Game/Environment/Materials/M_Player1Outline") : TEXT("/Game/Environment/Materials/M_Player2Outline");
	UMaterialInstance *Blendable = Cast<UMaterialInstance>(StaticLoadObject(UMaterialInstance::StaticClass(), nullptr, BlendableName));
	Camera->PostProcessSettings.AddBlendable(Blendable, 1.f);
	InternalCamera->PostProcessSettings.AddBlendable(Blendable, 1.f);
}

int AShoppingCartsPawn::GetId() const
{
	APlayerController* MyController = Cast<APlayerController>(GetController());
	return UGameplayStatics::GetPlayerControllerID(MyController);
}

APawn *AShoppingCartsPawn::GetEnemy() const
{
	int MyId = GetId();

	// Get the other player controller's pawn
	int EnemyId = MyId == 0 ? 1 : 0;
	APlayerController *EnemyController = UGameplayStatics::GetPlayerController(GetWorld(), EnemyId);
	return EnemyController ? EnemyController->GetPawn() : nullptr;
}

float AShoppingCartsPawn::GetSpeed() const
{
	FVector Velocity = GetMesh()->GetComponentVelocity();
	Velocity.Z = 0; // only horizontal velocity
	return FMath::Abs(Velocity.Size()) * 0.036f;
}

float AShoppingCartsPawn::TakeDamage(float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (ActualDamage > 0)
	{
		Health -= ActualDamage;
		if (Health <= 0)
		{
			Health = 0;
			UE_LOG(LogClass, Warning, TEXT("Cart died :("));

			// Don't hurt me anymore!
			bCanBeDamaged = false;

			// Stop vehicling
			GetVehicleMovementComponent()->SetThrottleInput(0);
			GetVehicleMovementComponent()->SetSteeringInput(0);
			GetVehicleMovementComponent()->Deactivate(); // TODO: remove this for respawning correctly?

			// Kaboom
			FVector EffectSpawnLocation = GetActorLocation();
			GetWorld()->SpawnActor(DeathEffectClass, &EffectSpawnLocation);

			// Toss into air
			GetMesh()->AddImpulse(FVector::UpVector * 800.f, NAME_None, true);
			FRandomStream random;
			random.GenerateNewSeed();
			GetMesh()->AddAngularImpulse(random.GetUnitVector() * 20.f, NAME_None, true);

			FTimerHandle TimerHandle;
			GetWorldTimerManager().SetTimer(TimerHandle, this, &AShoppingCartsPawn::AfterDeath, 5.0f, false);
		}
	}
	return ActualDamage;
}

void AShoppingCartsPawn::OnResetVR()
{
#if HMD_MODULE_INCLUDED
	if (GEngine->HMDDevice.IsValid())
	{
		GEngine->HMDDevice->ResetOrientationAndPosition();
		InternalCamera->SetRelativeLocation(InternalCameraOrigin);
		GetController()->SetControlRotation(FRotator());
	}
#endif // HMD_MODULE_INCLUDED
}

void AShoppingCartsPawn::UpdateHUDStrings()
{
	float KPH = GetSpeed();
	int32 KPH_int = FMath::FloorToInt(KPH);

	// Using FText because this is display text that should be localizable
	SpeedDisplayString = FText::Format(LOCTEXT("SpeedFormat", "{0} km/h"), FText::AsNumber(KPH_int));

	if (bInReverseGear)
	{
		GearDisplayString = FText(LOCTEXT("ReverseGear", "R"));
	}
	else
	{
		int32 Gear = GetVehicleMovement()->GetCurrentGear();
		GearDisplayString = (Gear == 0) ? LOCTEXT("N", "N") : FText::AsNumber(Gear);
	}

	MassDisplayString = FText::Format(LOCTEXT("MassFormat", "{0} kg"), FText::AsNumber(GetVehicleMovementComponent()->Mass));
}

void AShoppingCartsPawn::OnCollect()
{
	//Collect food/baby parts
	if (ItemWeight)
	{
		if (WantedItems.Contains(ItemString))
		{
			if (GetVehicleMovementComponent())
			{
				GetVehicleMovementComponent()->Mass += ItemWeight;
				//GetMesh()->GetMass();

				GetVehicleMovementComponent()->DragCoefficient += ItemWeight / 20;
			}
			WantedItems.Remove(ItemString);

			// Add item to cart
			FString MeshName = FString::Printf(TEXT("Item%f"), GetWorld()->TimeSeconds); // this changes with each new item
			UStaticMeshComponent *ItemMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), *MeshName);
			ItemMesh->RegisterComponent();
			ItemMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, ItemSpawnSocketName);
			ItemMesh->SetStaticMesh(ItemSpawnMesh);
			ItemMesh->SetRelativeScale3D(FVector(0.2f));
			ItemMesh->SetMaterial(0, ItemMaterials[ItemString]);
			ItemMesh->SetAllMassScale(0.2f);
			ItemMesh->SetSimulatePhysics(true);
		}
	}
}

void AShoppingCartsPawn::CollectPowerup(UPowerupType *InPowerupType)
{
	// Don't create powerup if it already exists
	if ((InPowerupType->CartPowerupComponentClass == UBoostPowerupComponent::StaticClass() && BoostPowerup) ||
		(InPowerupType->CartPowerupComponentClass == UTurretPowerupComponent::StaticClass() && TurretPowerup) ||
		(InPowerupType->CartPowerupComponentClass == UMissilePowerupComponent::StaticClass() && MissilePowerup))
	{
		return;
	}

	// Create component
	UCartPowerupComponent *Powerup = NewObject<UCartPowerupComponent>(this, InPowerupType->CartPowerupComponentClass, InPowerupType->Name);
	Powerup->SetupAttachment(GetRootComponent());
	Powerup->RegisterComponent();

	if (InPowerupType->CartPowerupComponentClass == UBoostPowerupComponent::StaticClass())
	{
		BoostPowerup = Cast<UBoostPowerupComponent>(Powerup);
	}
	else if (InPowerupType->CartPowerupComponentClass == UTurretPowerupComponent::StaticClass())
	{
		TurretPowerup = Cast<UTurretPowerupComponent>(Powerup);
		if (MissilePowerup)
		{
			MissilePowerup->DestroyComponent();
			MissilePowerup = nullptr;
		}
	}
	else if (InPowerupType->CartPowerupComponentClass == UMissilePowerupComponent::StaticClass())
	{
		MissilePowerup = Cast<UMissilePowerupComponent>(Powerup);
		if (TurretPowerup)
		{
			TurretPowerup->DestroyComponent();
			TurretPowerup = nullptr;
		}
	}
}

void AShoppingCartsPawn::Populate()
{
	FRandomStream Random;
	Random.GenerateNewSeed();

	const size_t AllItemsCount = 32;
	static FString AllItems[AllItemsCount] = {
		TEXT("Apple"),
		TEXT("Banana"),
		TEXT("Bread"),
		TEXT("Broccoli"),
		TEXT("Butter"),
		TEXT("Cake"),
		TEXT("Candy"),
		TEXT("Cereal"),
		TEXT("Cheese"),
		TEXT("Chips"),
		TEXT("Coconut"),
		TEXT("Cookie"),
		TEXT("Donut"),
		TEXT("Durian"),
		TEXT("Eggs"),
		TEXT("Hamburger"),
		TEXT("Hot Dog"),
		TEXT("Ice Cream"),
		TEXT("Juice"),
		TEXT("Leek"),
		TEXT("Lettuce"),
		TEXT("Mango"),
		TEXT("Milk"),
		TEXT("Onion"),
		TEXT("Orange"),
		TEXT("Pear"),
		TEXT("Rice"),
		TEXT("Spinach"),
		TEXT("Tomato"),
		TEXT("Waffle"), 
		TEXT("Watermelon"),
		TEXT("Yogurt")
	};

	if (ItemMaterials.Num() == 0)
	{
		// this is beautiful
		ItemMaterials.Add(TEXT("Apple"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Apple_Mat")).Object);
		ItemMaterials.Add(TEXT("Banana"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Banana_Mat")).Object);
		ItemMaterials.Add(TEXT("Bread"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Bread_Mat")).Object);
		ItemMaterials.Add(TEXT("Broccoli"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Broccoli_Mat")).Object);
		ItemMaterials.Add(TEXT("Butter"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Butter_Mat")).Object);
		ItemMaterials.Add(TEXT("Cake"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Cake_Mat")).Object);
		ItemMaterials.Add(TEXT("Candy"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Candy_Mat")).Object);
		ItemMaterials.Add(TEXT("Cereal"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Cereal_Mat")).Object);
		ItemMaterials.Add(TEXT("Cheese"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Cheese_Mat")).Object);
		ItemMaterials.Add(TEXT("Chips"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Chips_Mat")).Object);
		ItemMaterials.Add(TEXT("Coconut"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Coconut_Mat")).Object);
		ItemMaterials.Add(TEXT("Cookie"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Cookie_Mat")).Object);
		ItemMaterials.Add(TEXT("Donut"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Donut_Mat")).Object);
		ItemMaterials.Add(TEXT("Durian"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Durian_Mat")).Object);
		ItemMaterials.Add(TEXT("Eggs"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Eggs_Mat")).Object);
		ItemMaterials.Add(TEXT("Hamburger"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Hamburger_Mat")).Object);
		ItemMaterials.Add(TEXT("Hot Dog"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/HotDog_Mat")).Object);
		ItemMaterials.Add(TEXT("Ice Cream"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/IceCream_Mat")).Object);
		ItemMaterials.Add(TEXT("Juice"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Juice_Mat")).Object);
		ItemMaterials.Add(TEXT("Leek"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Leek_Mat")).Object);
		ItemMaterials.Add(TEXT("Lettuce"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Lettuce_Mat")).Object);
		ItemMaterials.Add(TEXT("Mango"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Mango_Mat")).Object);
		ItemMaterials.Add(TEXT("Milk"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Milk_Mat")).Object);
		ItemMaterials.Add(TEXT("Onion"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Onion_Mat")).Object);
		ItemMaterials.Add(TEXT("Orange"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Orange_Mat")).Object);
		ItemMaterials.Add(TEXT("Pear"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Pear_Mat")).Object);
		ItemMaterials.Add(TEXT("Rice"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Rice_Mat")).Object);
		ItemMaterials.Add(TEXT("Spinach"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Spinach_Mat")).Object);
		ItemMaterials.Add(TEXT("Tomato"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Tomato_Mat")).Object);
		ItemMaterials.Add(TEXT("Waffle"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Waffle_Mat")).Object);
		ItemMaterials.Add(TEXT("Watermelon"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Watermelon_Mat")).Object);
		ItemMaterials.Add(TEXT("Yogurt"), ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("/Game/Images/Yogurt_Mat")).Object);
	}

	while (WantedItems.Num() < 5)
	{
		WantedItems.AddUnique(AllItems[Random.RandRange(0, AllItemsCount - 1)]);
	}
}

void AShoppingCartsPawn::RemovePowerupComponent(UCartPowerupComponent* Component)
{
	if (Component)
	{
		if (Component->IsA<UBoostPowerupComponent>())
		{
			BoostPowerup = nullptr;
		}
		else if (Component->IsA<UTurretPowerupComponent>())
		{
			TurretPowerup = nullptr;
		}
		else if (Component->IsA<UMissilePowerupComponent>())
		{
			MissilePowerup = nullptr;
		}
	}
}

void AShoppingCartsPawn::AfterDeath()
{
	Health = 100.f;
	//Blowup = false;
	bCanBeDamaged = true;

	FRandomStream random;
	random.GenerateNewSeed();

	int sign = random.GetUnsignedInt() % 2 ? 1 : -1;
	if (FVector::DistSquaredXY(Cast<AShoppingCartsPawn>(GetEnemy())->GetActorLocation(), FVector(sign * 12000, 0.f, 300.f)) > 100.f)
		SetActorLocation(FVector(sign * 12000.f, 0.f, 300.f), false, nullptr, ETeleportType::TeleportPhysics);
	else
		SetActorLocation(FVector(-sign * 12000.f, 0.f, 300.f), false, nullptr, ETeleportType::TeleportPhysics);
	SetActorRotation(FRotator::ZeroRotator, ETeleportType::TeleportPhysics);
	GetMesh()->SetPhysicsLinearVelocity(FVector(0.f,0.f,GetMesh()->GetPhysicsLinearVelocity().Z));
	//GetMesh()->SetPhysicsLinearVelocity(FVector::ZeroVector);
	GetVehicleMovementComponent()->Activate();
}

void AShoppingCartsPawn::MainMenu()
{
	UGameplayStatics::OpenLevel(GetWorld(), "Main Menu", true);
	UGameplayStatics::RemovePlayer(UGameplayStatics::GetPlayerController(GetWorld(), 2), true);
}
#undef LOCTEXT_NAMESPACE

#pragma once

#include "Engine/GameViewportClient.h"
#include "LocalMPViewportClient.generated.h"

/**
 * See https://wiki.unrealengine.com/Local_Multiplayer_Tips
 */
UCLASS()
class SHOPPINGCARTS_API ULocalMPViewportClient : public UGameViewportClient
{
	GENERATED_BODY()
	
public:
	bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
	bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples = 1, bool bGamepad = false) override;
};

#pragma once

#include "CartPowerupComponent.h"
#include "TurretPowerupComponent.generated.h"

UCLASS()
class SHOPPINGCARTS_API UTurretPowerupComponent : public UCartPowerupComponent
{
	GENERATED_BODY()
	
	UTurretPowerupComponent();

	void BeginPlay() override;
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	void StartFire();
	void StopFire();

	UPROPERTY(Category = Turret, EditAnywhere)
	float FireRate = 0.1f;

	UPROPERTY(Category = Turret, EditAnywhere)
	float Damage = 3.0f;

	UPROPERTY(Category = Turret, EditAnywhere)
	float HitForce = 2000000.0f;

	UPROPERTY(Category = Turret, EditAnywhere)
	float Range = 10000.f;

	UPROPERTY(Category = Turret, EditAnywhere)
	float WeaponTurnLimit = 30.f;

	UPROPERTY(Category = Turret, EditAnywhere)
	USkeletalMesh *TurretMesh;

	UPROPERTY(Category = Turret, EditAnywhere)
	FName MuzzleSocket;

	UPROPERTY(Category = Turret, EditDefaultsOnly)
	UParticleSystem* MuzzleParticlesTemplate;

	UPROPERTY(Category = Turret, EditDefaultsOnly)
	UParticleSystem* HitParticlesTemplate;

	UPROPERTY(Category = Turret, EditDefaultsOnly)
	class USoundCue* FireLoopSound;

	UPROPERTY(Category = Turret, EditDefaultsOnly)
	class USoundCue* FireFinishSound;

private:
	void WeaponTrace();

	UPROPERTY(Transient)
	UAudioComponent* FireAudioComponent;

	UPROPERTY(Transient)
	USkeletalMeshComponent *LeftMeshComponent;
	UPROPERTY(Transient)
	USkeletalMeshComponent *RightMeshComponent;

	APawn *Enemy = nullptr;
	FTimerHandle WeaponTimer;

	int FireCount = 0;
};

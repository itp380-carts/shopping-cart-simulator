#pragma once

#include "Engine/TriggerBox.h"
#include "MyTriggerBox.generated.h"

UCLASS()
class SHOPPINGCARTS_API AMyTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

	void BeginPlay() override;
	
	//UPROPERTY(EditAnywhere)
	//class UItem* MyItem;

	UPROPERTY(EditAnywhere)
	FString MyName;

	UPROPERTY(EditAnywhere)
	int MyWeight;

	UPROPERTY(EditAnywhere)
	AActor* MyShelf;

	UFUNCTION()
	void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UFUNCTION()
	void NotifyActorEndOverlap(AActor* OtherActor) override;
	
private:
	void UpdateShelfVisibility();

};

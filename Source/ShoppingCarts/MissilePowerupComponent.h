#pragma once

#include "CartPowerupComponent.h"
#include "MissilePowerupComponent.generated.h"

UCLASS()
class SHOPPINGCARTS_API UMissilePowerupComponent : public UCartPowerupComponent
{
	GENERATED_BODY()
	
	UMissilePowerupComponent();

	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	void Launch();

	UPROPERTY(Category = Missile, EditAnywhere)
	float FireRate = 2.f;

	UPROPERTY(Category = Missile, EditAnywhere)
	UStaticMesh *LauncherMesh;

	UPROPERTY(Category = Missile, EditAnywhere)
	FName MuzzleSocket1;
	
	UPROPERTY(Category = Missile, EditAnywhere)
	FName MuzzleSocket2;

	UPROPERTY(Category = Missile, EditAnywhere)
	UParticleSystem* LaunchParticlesTemplate;

	UPROPERTY(Category = Missile, EditAnywhere)
	class USoundCue* LaunchSound;

private:
	UPROPERTY(Transient)
	UStaticMeshComponent *LauncherMeshComponent;

	int FireCount = 0;
	float LastFireTime = 0;
	
};

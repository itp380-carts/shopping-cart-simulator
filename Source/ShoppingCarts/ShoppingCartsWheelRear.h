// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "ShoppingCartsWheelRear.generated.h"

UCLASS()
class UShoppingCartsWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UShoppingCartsWheelRear();
};




#pragma once

#include "Components/ActorComponent.h"
#include "CartPowerupComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOPPINGCARTS_API UCartPowerupComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCartPowerupComponent();

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "ShoppingCarts.h"
#include "TurretPowerupComponent.h"
#include "ShoppingCartsPawn.h"

UTurretPowerupComponent::UTurretPowerupComponent()
{
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> mesh(TEXT("/Game/Weapons/StonerGun.StonerGun"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> muzzleParticles(TEXT("/Game/Effects/ParticleSystems/Weapons/AssaultRifle/Muzzle/P_AssaultRifle_Muzzle_OneShot"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> hitParticles(TEXT("/Game/Effects/ParticleSystems/Weapons/AssaultRifle/Impacts/P_AssaultRifle_IH"));
	static ConstructorHelpers::FObjectFinder<USoundCue> fireSound(TEXT("/Game/Sounds/Weapon_AssaultRifle/AssaultRifle_ShotLoop_Cue"));
	static ConstructorHelpers::FObjectFinder<USoundCue> finishSound(TEXT("/Game/Sounds/Weapon_AssaultRifle/AssaultRifle_ShotEnd_Cue"));

	TurretMesh = mesh.Object;
	MuzzleSocket = TEXT("MuzzleFlashSocket");
	MuzzleParticlesTemplate = muzzleParticles.Object;
	HitParticlesTemplate = hitParticles.Object;
	FireLoopSound = fireSound.Object;
	FireFinishSound = finishSound.Object;
}

void UTurretPowerupComponent::BeginPlay()
{
	Super::BeginPlay();

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());
	check(Cart);

	// Create the meshes as children of the root component
	LeftMeshComponent = NewObject<USkeletalMeshComponent>(GetOwner()->GetRootComponent(), USkeletalMeshComponent::StaticClass(), TEXT("LeftTurretMesh"));
	LeftMeshComponent->SetSkeletalMesh(TurretMesh);
	LeftMeshComponent->RegisterComponent();

	RightMeshComponent = NewObject<USkeletalMeshComponent>(GetOwner()->GetRootComponent(), USkeletalMeshComponent::StaticClass(), TEXT("RightTurretMesh"));
	RightMeshComponent->SetSkeletalMesh(TurretMesh);
	RightMeshComponent->RegisterComponent();

	// Attach the meshes to the sockets
	LeftMeshComponent->AttachToComponent(Cart->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("LeftTurretSocket"));
	RightMeshComponent->AttachToComponent(Cart->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RightTurretSocket"));
}

void UTurretPowerupComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!Enemy)
	{
		Enemy = Cast<AShoppingCartsPawn>(GetOwner())->GetEnemy();
	}

	if (Enemy)
	{
		// Rotate the weapons to face the target within a limited angle
		for (int i = 0; i < 2; ++i) {
			USkeletalMeshComponent *WeaponMeshComponent = i == 0 ? LeftMeshComponent : RightMeshComponent;

			FVector CurrentForward = GetOwner()->GetActorForwardVector();
			FVector NewForward = Enemy->GetActorLocation() - WeaponMeshComponent->GetSocketLocation(MuzzleSocket);
			NewForward.Normalize();

			// Clamp the new weapon forward to be within a cone of WeaponTurnLimit from the cart forward vector
			float AngleDegrees = FMath::RadiansToDegrees(FMath::Acos(CurrentForward | NewForward));
			AngleDegrees = FMath::Min(AngleDegrees, WeaponTurnLimit);
			NewForward = CurrentForward.RotateAngleAxis(AngleDegrees, CurrentForward ^ NewForward); // rotate along the plane between current and new

			// TODO: smooth the turning

			WeaponMeshComponent->SetWorldRotation(NewForward.Rotation());
		}
	}
}

void UTurretPowerupComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (LeftMeshComponent)
	{
		LeftMeshComponent->DestroyComponent();
		LeftMeshComponent = nullptr;
	}
	if (RightMeshComponent)
	{
		RightMeshComponent->DestroyComponent();
		RightMeshComponent = nullptr;
	}
	if (FireAudioComponent)
	{
		FireAudioComponent->Stop();
		FireAudioComponent->DestroyComponent();
		FireAudioComponent = nullptr;
	}
}

void UTurretPowerupComponent::StartFire()
{
	GetOwner()->GetWorldTimerManager().SetTimer(WeaponTimer, this, &UTurretPowerupComponent::WeaponTrace, FireRate, true);
	WeaponTrace();

	if (FireLoopSound)
	{
		FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireLoopSound, this);
	}
}

void UTurretPowerupComponent::StopFire()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(WeaponTimer);

	if (FireAudioComponent)
	{
		FireAudioComponent->Stop();
	}
	if (FireFinishSound)
	{
		FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireFinishSound, this);
	}
}

void UTurretPowerupComponent::WeaponTrace()
{
	static FName WeaponFireTag = FName(TEXT("UTurretPowerupComponent::WeaponTrace"));

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());

	bool UseLeftWeapon = FireCount % 2 == 0;

	// Shoot alternating weapon sides
	USkeletalMeshComponent *WeaponMeshComponent = UseLeftWeapon ? LeftMeshComponent : RightMeshComponent;

	// Calculate the ray
	FVector startPos = WeaponMeshComponent->GetSocketLocation(MuzzleSocket);
	FVector forward = WeaponMeshComponent->GetForwardVector();
	FVector endPos = startPos + forward * Range;

	// Configure trace to retrieve hit
	FCollisionQueryParams traceParams(WeaponFireTag, true, Cart->Instigator);
	traceParams.bTraceAsyncScene = true;

	// Fire the ray and check for collision
	FHitResult hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(hit, startPos, endPos, FCollisionObjectQueryParams::AllObjects, traceParams);
	if (hit.bBlockingHit)
	{
		// Show hit particles
		UGameplayStatics::SpawnEmitterAtLocation(this, HitParticlesTemplate, hit.ImpactPoint);

		if (hit.GetActor() == Enemy && Enemy)
		{
			Enemy->TakeDamage(Damage, FDamageEvent(), Cart->GetInstigatorController(), Cart);

			UPrimitiveComponent *EnemyRoot = Cast<UPrimitiveComponent>(Enemy->GetRootComponent());
			EnemyRoot->AddForceAtLocation(forward * HitForce, hit.ImpactPoint);
		}
	}

	// TODO: show line trace?

	// Show flash particles
	UParticleSystemComponent* MuzzleParticles = UGameplayStatics::SpawnEmitterAttached(MuzzleParticlesTemplate, UseLeftWeapon ? LeftMeshComponent : RightMeshComponent, MuzzleSocket);

	++FireCount;
}

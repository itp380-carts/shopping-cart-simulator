#include "ShoppingCarts.h"
#include "BoostPowerupComponent.h"
#include "ShoppingCartsPawn.h"

UBoostPowerupComponent::UBoostPowerupComponent()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> particleSystem(TEXT("/Game/Powerups/Boost/PS_Nitro"));
	NitroParticlesTemplate = particleSystem.Object;
}

void UBoostPowerupComponent::BeginPlay()
{
	Super::BeginPlay();

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());
	check(Cart);

	// Create the particle systems as children of the root component
	LeftParticleSystem = NewObject<UParticleSystemComponent>(GetOwner()->GetRootComponent(), UParticleSystemComponent::StaticClass(), TEXT("LeftParticleSystem"));
	LeftParticleSystem->SetTemplate(NitroParticlesTemplate);
	LeftParticleSystem->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	LeftParticleSystem->SetActive(false);
	LeftParticleSystem->RegisterComponent();

	RightParticleSystem = NewObject<UParticleSystemComponent>(GetOwner()->GetRootComponent(), UParticleSystemComponent::StaticClass(), TEXT("RightParticleSystem"));
	RightParticleSystem->SetTemplate(NitroParticlesTemplate);
	RightParticleSystem->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	RightParticleSystem->SetActive(false);
	RightParticleSystem->RegisterComponent();

	// Attach the particles to the sockets
	LeftParticleSystem->AttachToComponent(Cart->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("LeftWheelSocket"));
	RightParticleSystem->AttachToComponent(Cart->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RightWheelSocket"));
}

void UBoostPowerupComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());

	if (Cart)
	{
		UCameraComponent* Camera = Cart->GetCamera();

		if (bIsBoosting && BoostLevel > 0)
		{
			// Boost by adding an impuse forward
			FVector force = Cart->GetActorForwardVector() * BoostImpulseMagnitude;
			Cart->GetMesh()->AddForce(force, NAME_None, true);

			Camera->FieldOfView = FMath::Lerp(Camera->FieldOfView, BoostFOV, BoostFOVDamping * DeltaTime);

			BoostLevel -= BoostConsumptionRate * DeltaTime;
		}
		else
		{
			bIsBoosting = false;
			Camera->FieldOfView = FMath::Lerp(Camera->FieldOfView, 70.f, BoostFOVDamping * DeltaTime);

			if (BoostLevel < MaxBoostLevel)
			{
				BoostLevel = FMath::Min(BoostLevel + BoostRechargeRate * DeltaTime, MaxBoostLevel);
			}
		}
	}

	// NITRO!!!
	LeftParticleSystem->SetActive(bIsBoosting);
	RightParticleSystem->SetActive(bIsBoosting);
}

void UBoostPowerupComponent::SetIsBoosting(bool InBoosting)
{
	if (BoostLevel > BoostConsumptionRate * 1.f || !InBoosting) // 1s of boosting
	{
		bIsBoosting = InBoosting;
	}
}

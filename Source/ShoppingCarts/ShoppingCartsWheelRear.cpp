// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShoppingCarts.h"
#include "ShoppingCartsWheelRear.h"
#include "ShoppingCartsTireType.h"

UShoppingCartsWheelRear::UShoppingCartsWheelRear()
{
	ShapeRadius = 10.f;
	ShapeWidth = 5.f;
	bAffectedByHandbrake = true;
	SteerAngle = 0.f;
	TireType = CreateDefaultSubobject<UShoppingCartsTireType>("TireType");
}

#pragma once

#include "GameFramework/Actor.h"
#include "Missile.generated.h"

UCLASS()
class SHOPPINGCARTS_API AMissile : public AActor
{
	GENERATED_BODY()
	
	AMissile();

	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void Destroy();

public:
	UPROPERTY(Category = Missile, EditAnywhere)
	float Damage = 10.f;

	UPROPERTY(Category = Missile, EditAnywhere)
	float Lifetime = 3.f;

	UPROPERTY(Category = Missile, EditAnywhere)
	UParticleSystem *ExplosionParticleTemplate;

	void SetTarget(APawn *Target);

private:
	APawn *Target;

private_subobject:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent *Mesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UProjectileMovementComponent *ProjectileMovement;
};

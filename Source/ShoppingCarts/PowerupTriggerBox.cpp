#include "ShoppingCarts.h"
#include "PowerupTriggerBox.h"
#include "ShoppingCartsPawn.h"

APowerupTriggerBox::APowerupTriggerBox()
{
	SetActorHiddenInGame(false);
	GetCollisionComponent()->bDrawOnlyIfSelected = true;
	GetSpriteComponent()->SetVisibility(false);

	HideWhenCollected = CreateDefaultSubobject<USceneComponent>("HideWhenCollected");
	HideWhenCollected->SetupAttachment(RootComponent);
}

void APowerupTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(OtherActor);
	if (Cart)
	{
		Cart->CollectPowerup(PowerupType);

		// Deactivate until next time
		HideWhenCollected->SetVisibility(false, true);
		if (!GetWorldTimerManager().IsTimerActive(CollectTimerHandle))
		{
			GetWorldTimerManager().SetTimer(CollectTimerHandle, this, &APowerupTriggerBox::ShowCollected, HideTime);
		}
	}
}

void APowerupTriggerBox::ShowCollected()
{
	HideWhenCollected->SetVisibility(true, true);
}

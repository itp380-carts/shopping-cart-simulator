// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShoppingCarts.h"
#include "ShoppingCartsGameMode.h"
#include "ShoppingCartsPawn.h"
#include "ShoppingCartsHud.h"

AShoppingCartsGameMode::AShoppingCartsGameMode()
{
	DefaultPawnClass = AShoppingCartsPawn::StaticClass();
	HUDClass = AShoppingCartsHud::StaticClass();
	
}

void AShoppingCartsGameMode::BeginPlay()
{
	// Spawn a second player
	if (!UGameplayStatics::GetPlayerController(GetWorld(), 1))
	{
		APlayerController* player2 = UGameplayStatics::CreatePlayer(GetWorld());
		if (player2)
		{
			ACharacter* character = player2->GetCharacter();
			if (character)
			{
				character->AutoPossessPlayer = EAutoReceiveInput::Player1;
			}
		}
	}
}

AActor* AShoppingCartsGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	// See AGameMode::ChoosePlayerStart_Implementation.
	// A random choice of spawn point means sometimes we get:
	//   LogSpawn:Warning: SpawnActor failed because of collision at the spawn location ...
	//   LogGameMode:Warning: Couldn't spawn Pawn of type ShoppingCartsPawn at PlayerStart_1
	// because the two pawns choose the same PlayerStart for some reason.
	// This implementation forces choosing each of the PlayerSpawns in order, preventing overlaps.

	// Get all of the PlayerStarts
	if (PlayerStarts.Num() == 0)
	{
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStarts);
	}

	// Choose each start in order
	return PlayerStarts[ChoosePlayerStartCount++ % PlayerStarts.Num()];
}
#include "ShoppingCarts.h"
#include "MyTriggerBox.h"
#include "ShoppingCartsPawn.h"

void AMyTriggerBox::BeginPlay()
{
	Super::BeginPlay();

	UpdateShelfVisibility();
}

void AMyTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if (OtherActor && (OtherActor != this) && OtherActor->IsA(AShoppingCartsPawn::StaticClass()))
	{
		Cast<AShoppingCartsPawn>(OtherActor)->SetIsColliding(true);
		Cast<AShoppingCartsPawn>(OtherActor)->ItemString = MyName;
		Cast<AShoppingCartsPawn>(OtherActor)->ItemWeight = MyWeight;
	}
}

void AMyTriggerBox::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);
	if (OtherActor && (OtherActor != this) && OtherActor->IsA(AShoppingCartsPawn::StaticClass()))
	{
		Cast<AShoppingCartsPawn>(OtherActor)->SetIsColliding(false);
		Cast<AShoppingCartsPawn>(OtherActor)->ItemString = TEXT("");
		Cast<AShoppingCartsPawn>(OtherActor)->ItemWeight = 0;

		UpdateShelfVisibility();
	}
}

void AMyTriggerBox::UpdateShelfVisibility()
{
	if (MyShelf)
	{
		// Get player pawns
		AShoppingCartsPawn *Carts[] = { nullptr, nullptr };
		for (int i = 0; i < 2; ++i)
		{
			APlayerController *PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), i);
			if (PlayerController)
			{
				Carts[i] = Cast<AShoppingCartsPawn>(PlayerController->GetPawn());
			}
		}

		int cart1Value = Carts[0] && Carts[0]->WantedItems.Contains(MyName) ? 1 : 0;
		int cart2Value = Carts[1] && Carts[1]->WantedItems.Contains(MyName) ? 2 : 0;

		UStaticMeshComponent *ShelfMesh = Cast<UStaticMeshComponent>(MyShelf->GetRootComponent());
		if (ShelfMesh)
		{
			ShelfMesh->SetRenderCustomDepth(true);
			ShelfMesh->SetCustomDepthStencilValue(cart1Value | cart2Value); // 1 if 1, 2 if 2, 3 if both
		}
	}
}

// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "ShoppingCartsHud.generated.h"


UCLASS(config = Game)
class AShoppingCartsHud : public AHUD
{
	GENERATED_BODY()

public:
	AShoppingCartsHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface
};

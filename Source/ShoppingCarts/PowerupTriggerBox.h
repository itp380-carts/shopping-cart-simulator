#pragma once

#include "Engine/TriggerBox.h"
#include "PowerupTriggerBox.generated.h"

UCLASS()
class SHOPPINGCARTS_API APowerupTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

	APowerupTriggerBox();
	
	void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:
	UPROPERTY(Category = Powerup, EditAnywhere)
	class UPowerupType *PowerupType;

	UPROPERTY(Category = Powerup, EditAnywhere)
	float HideTime = 5.f;

private:
	void ShowCollected();

	FTimerHandle CollectTimerHandle;

private_subobject:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	USceneComponent *HideWhenCollected;
};

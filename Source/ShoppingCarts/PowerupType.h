#pragma once

#include "Engine/DataAsset.h"
#include "PowerupType.generated.h"

UCLASS()
class SHOPPINGCARTS_API UPowerupType : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Category = Powerup, EditAnywhere)
	FName Name;

	UPROPERTY(Category = Powerup, EditAnywhere)
	TSubclassOf<class UCartPowerupComponent> CartPowerupComponentClass;

};

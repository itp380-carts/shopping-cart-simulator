#pragma once

#include "Vehicles/TireType.h"
#include "ShoppingCartsTireType.generated.h"

UCLASS()
class SHOPPINGCARTS_API UShoppingCartsTireType : public UTireType
{
	GENERATED_BODY()

	UShoppingCartsTireType();
};

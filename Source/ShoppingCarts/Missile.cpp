#include "ShoppingCarts.h"
#include "Missile.h"

AMissile::AMissile()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(TEXT("/Game/Powerups/Missile/Missile"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleFinder(TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> SmokeParticleFinder(TEXT("/Game/Powerups/Missile/P_MissileSmoke"));

	ExplosionParticleTemplate = ExplosionParticleFinder.Object;

	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetStaticMesh(MeshFinder.Object);
	Mesh->SetSimulatePhysics(false);
	Mesh->BodyInstance.bNotifyRigidBodyCollision = true;
	Mesh->BodyInstance.bUseCCD = true;
	Mesh->bGenerateOverlapEvents = true;
	Mesh->SetCanEverAffectNavigation(false);
	Mesh->OnComponentHit.AddDynamic(this, &AMissile::OnHit); // set up a notification for when this component hits something blocking
	RootComponent = Mesh;

	UParticleSystemComponent *Particles = CreateDefaultSubobject<UParticleSystemComponent>("Smoke");
	Particles->SetupAttachment(Mesh);
	Particles->SetTemplate(SmokeParticleFinder.Object);
	Particles->ActivateSystem(true);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	ProjectileMovement->UpdatedComponent = Mesh;
	ProjectileMovement->InitialSpeed = 1000.f;
	ProjectileMovement->MaxSpeed = 1000.f;
	ProjectileMovement->ProjectileGravityScale = 0; // no gravity!
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->bIsHomingProjectile = true;
	ProjectileMovement->HomingAccelerationMagnitude = 4000.f;
}

void AMissile::BeginPlay()
{
	Super::BeginPlay();

	Mesh->IgnoreActorWhenMoving(Instigator, true);

	// Destroy after some time
	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &AMissile::Destroy, Lifetime);
}

void AMissile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// Boom
	if (ExplosionParticleTemplate)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticleTemplate, GetActorLocation());
	}

	Super::EndPlay(EndPlayReason);
}

void AMissile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && OtherActor == Target)
	{
		// Damage the enemy
		Target->TakeDamage(Damage, FDamageEvent(), Instigator ? Instigator->GetInstigatorController() : nullptr, this);

		Destroy();
	}
}

void AMissile::Destroy()
{
	Super::Destroy();
}

void AMissile::SetTarget(APawn* Target)
{
	if (Target)
	{
		this->Target = Target;
		ProjectileMovement->HomingTargetComponent = Target->GetRootComponent();
	}
}

#include "ShoppingCarts.h"
#include "MissilePowerupComponent.h"
#include "ShoppingCartsPawn.h"
#include "Missile.h"

UMissilePowerupComponent::UMissilePowerupComponent()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> mesh(TEXT("/Game/Powerups/Missile/MissileLauncher"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> launchParticles(TEXT("/Game/Effects/ParticleSystems/Weapons/AssaultRifle/Muzzle/P_AssaultRifle_Muzzle_OneShot"));
	static ConstructorHelpers::FObjectFinder<USoundCue> launchSound(TEXT("/Game/Sounds/Weapon_AssaultRifle/AssaultRifle_ShotEnd_Cue"));

	LauncherMesh = mesh.Object;
	MuzzleSocket1 = TEXT("MuzzleRocketSocket");
	MuzzleSocket2 = TEXT("MuzzleRocketSocket2");
	LaunchParticlesTemplate = launchParticles.Object;
	LaunchSound = launchSound.Object;
}

void UMissilePowerupComponent::BeginPlay()
{
	Super::BeginPlay();

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());
	check(Cart);

	// Create the mesh as a child of the root component
	LauncherMeshComponent = NewObject<UStaticMeshComponent>(GetOwner()->GetRootComponent(), UStaticMeshComponent::StaticClass(), TEXT("MissileLauncherMesh"));
	LauncherMeshComponent->SetStaticMesh(LauncherMesh);
	LauncherMeshComponent->RegisterComponent();
	LauncherMeshComponent->AttachToComponent(Cart->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("SeatSocket"));
}

void UMissilePowerupComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (LauncherMeshComponent)
	{
		LauncherMeshComponent->DestroyComponent();
		LauncherMeshComponent = nullptr;
	}
}

void UMissilePowerupComponent::Launch()
{
	// Restrict to fire rate
	float Now = GetWorld()->GetRealTimeSeconds();
	if (Now - LastFireTime < FireRate)
	{
		return;
	}

	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());
	APawn *Enemy = Cart->GetEnemy();

	// Spawn missile
	FName &SocketName = (FireCount % 2 == 0) ? MuzzleSocket1 : MuzzleSocket2;
	AMissile *Missile = GetWorld()->SpawnActor<AMissile>(AMissile::StaticClass(),
			LauncherMeshComponent->GetSocketLocation(SocketName),
			LauncherMeshComponent->GetSocketRotation(SocketName));
	Missile->Instigator = Cart;
	Missile->SetTarget(Enemy);
	Missile->Damage = 25.f;

	// Show flash particles
	UParticleSystemComponent* MuzzleParticles = UGameplayStatics::SpawnEmitterAttached(LaunchParticlesTemplate, LauncherMeshComponent, SocketName);

	// Ear stuff
	UGameplayStatics::SpawnSoundAttached(LaunchSound, this);

	++FireCount;
	LastFireTime = Now;
}

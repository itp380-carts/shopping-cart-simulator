// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "ShoppingCartsWheelFront.generated.h"

UCLASS()
class UShoppingCartsWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UShoppingCartsWheelFront();
};




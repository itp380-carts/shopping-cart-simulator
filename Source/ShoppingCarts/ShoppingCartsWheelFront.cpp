// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShoppingCarts.h"
#include "ShoppingCartsWheelFront.h"
#include "ShoppingCartsTireType.h"

UShoppingCartsWheelFront::UShoppingCartsWheelFront()
{
	ShapeRadius = 10.f;
	ShapeWidth = 5.f;
	bAffectedByHandbrake = false;
	SteerAngle = 70.f;
	TireType = CreateDefaultSubobject<UShoppingCartsTireType>("TireType");
}

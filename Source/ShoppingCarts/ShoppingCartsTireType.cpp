#include "ShoppingCarts.h"
#include "ShoppingCartsTireType.h"

UShoppingCartsTireType::UShoppingCartsTireType()
{
	// Don't drift as easily
	SetFrictionScale(6.f);
}

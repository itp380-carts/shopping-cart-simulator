// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/WheeledVehicle.h"
#include "ShoppingCartsPawn.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UTextRenderComponent;
class UInputComponent;
UCLASS(config=Game)
class AShoppingCartsPawn : public AWheeledVehicle
{
	GENERATED_BODY()

	/** Spring arm that will offset the camera */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* SpringArm;

	/** Camera component that will be our viewpoint */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;

	/** SCene component for the In-Car view origin */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* InternalCameraBase;

	/** Camera component for the In-Car view */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* InternalCamera;


public:
	AShoppingCartsPawn();

	/** The current speed as a string eg 10 km/h */
	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
	FText SpeedDisplayString;

	/** The current gear as a string (R,N, 1,2 etc) */
	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
	FText GearDisplayString;

	/** The color of the incar gear text in forward gears */
	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
	FColor GearDisplayColor;

	/** The color of the incar gear text when in reverse */
	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
	FColor GearDisplayReverseColor;

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
	FText MassDisplayString;

	/** Are we using incar camera */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
	bool bInCarCameraActive;

	/** Are we in reverse gear */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
	bool bInReverseGear;

	UPROPERTY(Category = Vehicle, EditAnywhere)
	TSubclassOf<AActor> DeathEffectClass;

	UPROPERTY(Category = Vehicle, EditAnywhere)
	float RespawnUpOffset = 200.f;

	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly)
	float Health = 100.f;

	/*Added Variables*/
	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly)
	FString ItemString;

	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly)
	TArray<FString> WantedItems;

	UPROPERTY(Category = Vehicle, EditAnywhere)
	UStaticMesh *ItemSpawnMesh;

	UPROPERTY(Category = Vehicle, EditAnywhere)
	class USoundCue* EngineSound;

	int ItemWeight;

	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly)
	bool Victory;
	//bool Blowup;

	UPROPERTY(Category = Vehicle, EditAnywhere)
	FName ItemSpawnSocketName = TEXT("ItemSpawnSocket");

	/** Initial offset of incar camera */
	FVector InternalCameraOrigin;
	// Begin Pawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End Pawn interface

	// Begin Actor interface
	virtual void Tick(float Delta) override;
	virtual void BeginPlay() override;
	// End Actor interface

	int GetId() const;
	UFUNCTION(Category = Cart, BlueprintCallable)
	APawn *GetEnemy() const;

	UFUNCTION(Category = Cart, BlueprintCallable)
	float GetSpeed() const;

	float TakeDamage(float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	/** Handle pressing forwards */
	void MoveForward(float Val);

	/** Update the physics material used by the vehicle mesh */
	void UpdatePhysicsMaterial();
	/** Handle pressing right */
	void MoveRight(float Val);
	/** Handle handbrake pressed */
	void OnHandbrakePressed();
	/** Handle handbrake released */
	void OnHandbrakeReleased();
	/** Switch between cameras */
	void OnToggleCamera();
	/** Handle reset VR device */
	void OnResetVR();

	void OnBoostStart();
	void OnBoostStop();
	void OnFireStart();
	void OnFireStop();
	void OnReset();
	void OnCollect();

	void CollectPowerup(class UPowerupType *InPowerupType);

	bool GetIsColliding() const { return IsColliding; }
	void SetIsColliding(bool InColliding) { IsColliding = InColliding; }

	void RemovePowerupComponent(class UCartPowerupComponent* Component);

	static const FName LookUpBinding;
	static const FName LookRightBinding;

	void AfterDeath();
	void MainMenu();

private:
	/**
	 * Activate In-Car camera. Enable camera and sets visibility of incar hud display
	 *
	 * @param	bState true will enable in car view and set visibility of various if its doesnt match new state
	 * @param	bForce true will force to always change state
	 */
	void EnableIncarView( const bool bState, const bool bForce = false );

	/** Update the gear and speed strings */
	void UpdateHUDStrings();

	void Populate();

	UAudioComponent *EngineAudioComponent;

protected:
	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly, Transient)
	class UBoostPowerupComponent *BoostPowerup;

	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly, Transient)
	class UTurretPowerupComponent *TurretPowerup;

	UPROPERTY(Category = Vehicle, VisibleAnywhere, BlueprintReadOnly, Transient)
	class UMissilePowerupComponent *MissilePowerup;

	/* Are we on a 'slippery' surface */
	bool bIsLowFriction;

	bool IsColliding;

public:
	/** Returns SpringArm subobject **/
	FORCEINLINE USpringArmComponent* GetSpringArm() const { return SpringArm; }
	/** Returns Camera subobject **/
	FORCEINLINE UCameraComponent* GetCamera() const { return Camera; }
	/** Returns InternalCamera subobject **/
	FORCEINLINE UCameraComponent* GetInternalCamera() const { return InternalCamera; }
};

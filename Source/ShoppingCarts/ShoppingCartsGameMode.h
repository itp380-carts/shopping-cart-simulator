// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ShoppingCartsGameMode.generated.h"

UCLASS(minimalapi)
class AShoppingCartsGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AShoppingCartsGameMode();

	void BeginPlay() override;

	class AActor* ChoosePlayerStart_Implementation(AController* Player) override;

private:
	TArray<AActor*> PlayerStarts;
	int ChoosePlayerStartCount = 0;

};

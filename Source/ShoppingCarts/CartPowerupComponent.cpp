#include "ShoppingCarts.h"
#include "CartPowerupComponent.h"
#include "ShoppingCartsPawn.h"


// Sets default values for this component's properties
UCartPowerupComponent::UCartPowerupComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}

void UCartPowerupComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// Remove from powerup list on cart
	AShoppingCartsPawn *Cart = Cast<AShoppingCartsPawn>(GetOwner());
	if (Cart)
	{
		Cart->RemovePowerupComponent(this);
	}

	Super::EndPlay(EndPlayReason);
}

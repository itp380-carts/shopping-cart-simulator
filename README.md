# Shopping Cart Simulator

## Credits
Cart model: http://archive3d.net/?a=download&id=b8ee8619
Gun: https://www.highend3d.com/3d-model/guns-pack-free-3d-model (MIT License)
Missile Launcher: https://www.cgtrader.com/free-3d-models/space/spacecraft-sci-fi/lrt10-dingo-laser-rocket-launcher
Rocket: https://www.cgtrader.com/free-3d-models/vehicle/military/abrams-3iae-missle-launcher
Missile Smoke: https://spritefx.blogspot.com/2013/04/sprite-smoke.html
